﻿using fsb.smartcontract.Services.Role;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Model.Requests;
using Model.Response;
using Model.Role;
using NToastNotify;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace fsb.smartcontract.Controllers
{
    public class RoleController : BaseController
    {
        private readonly ILogger<RoleController> _logger;
        private readonly IToastNotification _toastNotification;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IRoleService _roleService;

        public RoleController(
            ILogger<RoleController> logger,
            IHttpClientFactory factory,
            IToastNotification toastNotification,
            IHttpContextAccessor httpContextAccessor,
            IRoleService roleService
            ) : base(factory)
        {
            _logger = logger;
            _toastNotification = toastNotification;
            _httpContextAccessor = httpContextAccessor;
            _roleService = roleService;
        }

        [Route("danh-sach-nhom-quyen")]
        public IActionResult Roles()
        {
            return View();
        }

        [HttpPost]
        public IActionResult GetListRole(string searchValue)
        {
            SearchRoleModel searchModel = talbeSearchModel.Get<SearchRoleModel>();
            searchModel.SearchValue = searchValue;
            var data = _roleService.GetListRole(searchModel);
            if (data.Code != StatusCodes.Status200OK)
                _toastNotification.AddErrorToastMessage(data.Message);
            return Json(data);
        }

        [Route("them-moi-nhom-quyen")]
        public IActionResult CreateRole()
        {
            return View();
        }

        [HttpPost]
        public IActionResult InsertRole(RoleModel model)
        {
            var res = _roleService.CreateRole(model);
            if (res.Code == StatusCodes.Status200OK)
            {
                _toastNotification.AddSuccessToastMessage(res.Message);
            }
            else
            {
                _toastNotification.AddErrorToastMessage(res.Message);
            }
            return Json(res);
        }

        [HttpPost]
        public IActionResult DeleteRole(Guid Id)
        {
            var model = new RoleModel();
            model.Id = Id;
            model.UpdateBy = _SessionUser.Id;
            var res = _roleService.DeleteRole(model);
            if (res.Code == StatusCodes.Status200OK)
            {
                _toastNotification.AddSuccessToastMessage(res.Message);
            }
            else
            {
                _toastNotification.AddErrorToastMessage(res.Message);
            }
            return Json(res);
        }

        [Route("sua-nhom-quyen/{id}")]
        public IActionResult EditRole(Guid Id)
        {
            var model = new RoleModel();
            model.Id = Id;
            var res = _roleService.GetRoleById(model);

            if (res == null)
                res = new Response<RoleModel>();
            if (res.Data == null)
                res.Data = new RoleModel();

            return View(res.Data);
        }

        [HttpPost]
        public IActionResult UpdateRole(RoleModel model)
        {
            var res = _roleService.UpdateRole(model);
            if (res.Code == StatusCodes.Status200OK)
            {
                _toastNotification.AddSuccessToastMessage(res.Message);
            }
            else
            {
                _toastNotification.AddErrorToastMessage(res.Message);
            }
            return Json(res);
        }
    }
}
